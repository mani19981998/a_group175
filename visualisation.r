x <- data$ENVIRONMENT.CATEGORY.SCORE
y <- data$PERFORMANCE.CATEGORY.SCORE

plot(x
     , y
     , main = "ENV vs PER"
     , xlab ="Env"
     ,  ylab = "Per"
     , pch = 19
     , frame = T
)
model <- lm(y ~ x, data = data)
abline(model, col = "blue")

x <- data$ENVIRONMENT.CATEGORY.SCORE
y <- data$PERFORMANCE.CATEGORY.SCORE

plot(y ~ x
     , main = "ENV vs PER"
     , xlab =  "Env"
     ,  ylab = "Per"
     , pch = 19
     , frame = T
)
points(y ~ x, col="cyan");

data <- read.csv("https://query.data.world/s/7ae3whlyvlem7b45y3vskylr4ci2vl", header = TRUE)

y <- data$PERFORMANCE.CATEGORY.SCORE

h <- hist(y
          , 6
          , main = "Score Frequency"
          , xlab = "Performance"
          , ylab = "frequency"
          , col="azure"
)

mn <- mean(y)
stdD <- sd(y)
x <- seq(100, 700, 1)
y1 <- dnorm(x, mean=mn, sd=stdD)

y1 <- y1 * diff(h$mids[1:2]) * length(y);

lines(x, y1, col="blue")